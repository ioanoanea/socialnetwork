package service;

import domain.User;
import repository.base.Repository;
import result.Result;
import result.ResultSuccess;
import service.base.Service;

import java.io.IOException;

@SuppressWarnings("unchecked")
public class UserService extends Service<User> {

    public UserService(Repository<User> repository) {
        super(repository);
    }

    /**
     * Add new user
     * @param firstName - user's first name
     * @param lastName - user's last name
     * @param email - user's email
     * @param password - user's password
     * @return success status
     * @throws IOException if repository is FileRepository and fails saving data to file
     */
    public Result add(String firstName, String lastName, String email, String password) throws IOException {
        User lastEntry = repository.getLastEntry();
        Long lastId;
        if (lastEntry != null) {
            lastId = lastEntry.getId();
        } else {
            lastId = 0L;
        }
        User user = new User(lastId + 1, firstName, lastName, email, password);
        return repository.add(user);
    }

    /**
     * Update user's first name
     * @param id - the id of user to be updated
     * @param firstName - new first name
     * @return success status
     * @throws IOException if repository is FileRepository and fails saving data to file
     */
    public Result updateFirstName(Long id, String firstName) throws IOException {
        // Get user
        Result result = repository.get(id);
        // If result is success (user exists)
        if (result.isSuccess()) {
            User user = ((ResultSuccess<User>) result).getData();
            // Update first name
            user.setFirstName(firstName);
            return repository.update(user);
        }
        return result;
    }

    /**
     * Update user's last name
     * @param id - the id of user to be updated
     * @param lastName - new last name
     * @return success status
     * @throws IOException if repository is FileRepository and fails saving data to file
     */
    public Result updateLastName(Long id, String lastName) throws IOException {
        // Get user
        Result result = repository.get(id);
        // If result is success (user exists)
        if (result.isSuccess()) {
            User user = ((ResultSuccess<User>) result).getData();
            // Update last name
            user.setLastName(lastName);
            return repository.update(user);
        }
        return result;
    }

    /**
     * Update user's email
     * @param id - the id of user to be updated
     * @param email - new email
     * @return success status
     * @throws IOException if repository is FileRepository and fails saving data to file
     */
    public Result updateEmail(Long id, String email) throws IOException {
        // Get user
        Result result = repository.get(id);
        // If result is success (user exists)
        if (result.isSuccess()) {
            User user = ((ResultSuccess<User>) result).getData();
            // Update email
            user.setEmail(email);
            return repository.update(user);
        }
        return result;
    }

    /**
     * Update user's password
     * @param id - the id of user to be updated
     * @param password - new password
     * @return success status
     * @throws IOException if repository is FileRepository and fails saving data to file
     */
    public Result updatePassword(Long id, String password) throws IOException {
        // Get user
        Result result = repository.get(id);
        // If result is success (user exists)
        if (result.isSuccess()) {
            User user = ((ResultSuccess<User>) result).getData();
            // Update password
            user.setPassword(password);
            return repository.update(user);
        }
        return result;
    }
}
