package service.base;

import domain.Entity;
import repository.base.Repository;
import result.Result;

import java.io.IOException;

public class Service<E extends Entity> {

    protected final Repository<E> repository;

    public Service(Repository<E> repository) {
        this.repository = repository;
    }

    /**
     * Returns entity by id
     * @param id - the id of entity to be returned
     * @return - result success with the entity if exists, error result otherwise
     */
    public Result get(Long id) {
        return repository.get(id);
    }

    /**
     *
     * @return - result with all entities
     */
    public Result getAll() {
        return repository.getAll();
    }

    /**
     * Delete an entity by id
     * @param id - the id of entity to be deleted
     * @return -
     * @throws IOException if repository is FileRepository and fails saving data to file
     */
    public Result delete(Long id) throws IOException {
        return repository.delete(id);
    }
}
