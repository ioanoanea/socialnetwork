package result;

public class ResultError implements Result {

    private final String error;

    public ResultError(String error) {
        this.error = error;
    }

    @Override
    public boolean isSuccess() {
        return false;
    }

    public String getError() {
        return error;
    }
}
