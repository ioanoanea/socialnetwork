package result;

public interface Result {
    boolean isSuccess();
}
