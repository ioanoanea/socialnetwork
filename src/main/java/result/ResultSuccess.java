package result;

public class ResultSuccess<T> implements Result {

    private final T data;

    public ResultSuccess() {
        this.data = null;
    }
    public ResultSuccess(T data) {
        this.data = data;
    }

    @Override
    public boolean isSuccess() {
        return true;
    }

    public T getData() {
        return data;
    }
}
