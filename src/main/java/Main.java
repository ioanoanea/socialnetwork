import database.DBConnect;
import database.FriendshipDao;
import database.UserDao;
import domain.Friendship;
import domain.User;
import domain.validators.FriendshipValidator;
import domain.validators.UserValidator;
import repository.base.DatabaseRepository;
import service.FriendshipService;
import service.UserService;
import ui.Ui;

import java.sql.Connection;

public class Main {

    public static void main(String[] args) {
        UserValidator userValidator = new UserValidator();
        FriendshipValidator friendshipValidator = new FriendshipValidator();
        Connection connection = new DBConnect().connection();

        // insertUser(new User(1L, "test", "test", "test@gmail.com", "1"), connection);

        try {
            // Repository
            // UserFileRepository userFileRepository = new UserFileRepository(userValidator, "Users.json");
            DatabaseRepository<User> userDatabaseRepository = new DatabaseRepository<>(userValidator, new UserDao());
            // FriendshipFileRepository friendshipFileRepository = new FriendshipFileRepository(friendshipValidator, "Friendships.json");
            DatabaseRepository<Friendship> friendshipDatabaseRepository = new DatabaseRepository<>(friendshipValidator, new FriendshipDao());
            // Service
            UserService userService = new UserService(userDatabaseRepository);
            FriendshipService friendshipService = new FriendshipService(friendshipDatabaseRepository, userDatabaseRepository);
            // Ui
            Ui ui = new Ui(userService, friendshipService);
            ui.showMenu();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
