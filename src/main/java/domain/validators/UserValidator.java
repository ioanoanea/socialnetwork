package domain.validators;

import domain.User;
import domain.validators.base.ValidationException;
import domain.validators.base.Validator;

public class UserValidator implements Validator<User> {
    @Override
    public void validate(User user) throws ValidationException {
        if (user.getId() == null) {
            throw new ValidationException("User id must be not null!");
        }
        if (user.getFirstName() == null) {
            throw new ValidationException("First name should be not null!");
        }
        if (user.getLastName() == null) {
            throw new ValidationException("Last name should be not null!");
        }
        if (user.getEmail() == null) {
            throw new ValidationException("Email must be not null!");
        }
        if (user.getPassword() == null) {
            throw new ValidationException("Password must be not null");
        }
    }
}
