package repository.base;

import domain.Entity;
import domain.validators.base.Validator;

public abstract class Repository<E extends Entity> implements RepositoryInterface<E> {

    protected Validator<E> validator;

    public Repository(Validator<E> validator) {
        this.validator = validator;
    }
}
