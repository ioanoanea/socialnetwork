package repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.User;
import domain.validators.base.Validator;
import repository.base.FileRepository;

import java.io.IOException;
import java.util.ArrayList;

public class UserFileRepository extends FileRepository<User> {

    public UserFileRepository(Validator<User> validator, String file) throws IOException {
        super(validator, file);
    }

    @Override
    protected ArrayList<User> deserialize(ArrayList<Object> list) {
        return new ObjectMapper().convertValue(list, new TypeReference<>(){});
    }

}
